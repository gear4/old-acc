#include <cube.h>
#include "parse.h"

SVAR(welcomemsg, "Welcome :)");

vector<client *> clients;
vector<serverinfo *> servers;

client &addclient(int type) {
  client *c = NULL;
  loopv(clients) if (clients[i]->type == ST_EMPTY) {
    c = clients[i];
    break;
  }
  if (!c) {
    c = new client;
    c->num = clients.length();
    clients.add(c);
  }
  clientinfo *v = new clientinfo;
  c->info = new clientinfo;
  c->type = type;
  return *c;
}

void deleteclientinfo(void *ci) { delete (clientinfo *)ci; }
void delclient(client *c) {
  if (!c) return;
  switch (c->type) {
    case ST_TCPIP:
      c->peer->data = NULL;
      break;
    case ST_EMPTY:
      return;
  }
  c->type = ST_EMPTY;
  c->peer = NULL;
  if (c->info) {
    deleteclientinfo(c->info);
    c->info = NULL;
  }
}

clientinfo *getinfo(int i) {
  return !clients.inrange(i) || clients[i]->type == ST_EMPTY ? NULL : (clientinfo *)clients[i]->info;
}

const char *disconnectreason(int reason) {
  switch (reason) {
    case ACC_DISC_EOP:
      return "end of packet";
    case ACC_DISC_MSGERR:
      return "message error";
    case ACC_DISC_TIMEOUT:
      return "connection timed out";
    case ACC_DISC_OVERFLOW:
      return "overflow";
    case ACC_DISC_PROTOCOL:
      return "invalid protocol";
    default:
      return NULL;
  }
}

void disconnect_client(int n, int reason) {
  if (!clients.inrange(n) || clients[n]->type != ST_TCPIP) return;
  enet_peer_disconnect(clients[n]->peer, reason);
  delclient(clients[n]);
  const char *msg = disconnectreason(reason);
  string s;
  if (msg)
    conoutf("client (%s) disconnected because: %s", clients[n]->hostname, msg);
  else
    conoutf("client (%s) disconnected", clients[n]->hostname);
}

ENetPacket *sendf(int cn, int chan, const char *format, ...) {
  int exclude = -1;
  bool reliable = false;
  if (*format == 'r') {
    reliable = true;
    ++format;
  }
  packetbuf p(5000, reliable ? ENET_PACKET_FLAG_RELIABLE : 0);
  va_list args;
  va_start(args, format);
  while (*format) switch (*format++) {
      case 'x':
        exclude = va_arg(args, int);
        break;

      case 'v': {
        int n = va_arg(args, int);
        int *v = va_arg(args, int *);
        loopi(n) putint(p, v[i]);
        break;
      }

      case 'i': {
        int n = isdigit(*format) ? *format++ - '0' : 1;
        loopi(n) putint(p, va_arg(args, int));
        break;
      }
      case 'f': {
        int n = isdigit(*format) ? *format++ - '0' : 1;
        loopi(n) putfloat(p, (float)va_arg(args, double));
        break;
      }
      case 's':
        sendstring(va_arg(args, const char *), p);
        break;
      case 'm': {
        int n = va_arg(args, int);
        p.put(va_arg(args, uchar *), n);
        break;
      }
    }
  va_end(args);
  ENetPacket *packet = p.finalize();
  enet_peer_send(clients[cn]->peer, chan, packet);
  return packet->referenceCount > 0 ? packet : NULL;
}
void sendservinfo(clientinfo *ci) {
  sendf(ci->cn, 1, "rii", ACC_N_SERVINFO, ci->cn);
}

void clientconnect(int n, uint ip) {
  clientinfo *ci = getinfo(n);
  ci->cn = n;

  sendservinfo(ci);
}

char * ip_numtoaddr(unsigned long ipnum) {
  char *ret_str;
  char *cur_str;
  int octet[4];
  int num_chars_written, i;

  ret_str = (char *)malloc(sizeof(char) * 16);
  cur_str = ret_str;

  for (i = 0; i < 4; i++) {
      octet[3 - i] = ipnum % 256;
      ipnum >>= 8;
  }

  for (i = 0; i < 4; i++) {
    num_chars_written = sprintf(cur_str, "%d", octet[i]);
    cur_str += num_chars_written;
    if (i < 3) {
      cur_str[0] = '.';
      cur_str++;
    }
  }

  return ret_str;
}

void parsepacket(int sender, int chan, packetbuf &p) {
  // has to parse exactly each byte of the packet
  if (sender < 0 || p.packet->flags & ENET_PACKET_FLAG_UNSEQUENCED || chan > 2)
    return;
  char text[5000];
  int type;
  clientinfo *ci = sender >= 0 ? getinfo(sender) : NULL, *cq = ci, *cm = ci;
  if (ci) {
    static char text[5000];
    if (chan == 0)
      return;
    else if (chan != 1) {
      disconnect_client(sender, ACC_DISC_MSGERR);
      return;
    } else
      while (p.length() < p.maxlen) switch (getint(p)) {
          case ACC_N_CONNECT: {
            getstring(text, p);
            conoutf("new connection! %s %i", text, sender);
            int prtcl = getint(p);
            if (prtcl != PROTOCOL_NUM) {
              conoutf("invalid protocol version (client: %i; server: %i)",
                     prtcl, PROTOCOL_NUM);
              disconnect_client(sender, ACC_DISC_PROTOCOL);
              delclient(clients[sender]);
              return;
            }
            sendf(ci->cn, 1, "ris", ACC_N_CONNECT, welcomemsg);
          } break;
          case ACC_N_SERVCONN: {
            ulong ip;
            p.get((uchar*)&ip, 4);
            getint(p);
            int port = getint(p);
            conoutf("client %s connected to %s:%i", clients[sender]->hostname, ip_numtoaddr(ip), port);
            ci->connected = true;
          } break;
          case ACC_N_GUNSELECT:
            conoutf("gun changed to %i", getint(p));
            break;
          case ACC_N_SOUND:
            conoutf("sound played: %i", getint(p));
            break;
          case ACC_N_EDITMODE:
            conoutf("player switched to editmode: %i", getint(p));
            break;
          case ACC_N_NEWGAME:
            getstring(text, p);
            int mode = getint(p);
            conoutf("new game: %i on %s", mode, text);
            break;
        }
  }
}
void process(ENetPacket *packet, int sender, int chan) {
  // sender may be -1
  packetbuf p(packet);
  parsepacket(sender, chan, p);
  if (p.overread()) {
    disconnect_client(sender, ACC_DISC_EOP);
    return;
  }
}
