#define PROTOCOL_NUM (9)

enum {
  ACC_DISC_NONE = 0,
  ACC_DISC_EOP,
  ACC_DISC_MSGERR,
  ACC_DISC_TIMEOUT,
  ACC_DISC_OVERFLOW,
  ACC_DISC_PROTOCOL
};

enum { ST_EMPTY, ST_LOCAL, ST_TCPIP };

enum {
  ACC_N_CONNECT = 0,
  ACC_N_ERROR,
  ACC_N_SERVINFO,
  ACC_N_GUNSELECT,
  ACC_N_SOUND,
  ACC_N_EDITMODE,
  ACC_N_NEWGAME,
  ACC_N_SERVCONN,
  ACC_N_PING,
  ACC_N_PONG
};

struct serverentry {
  uint ip;
  int port;
};

struct serverinfo {
  char *cnlist;
  serverentry info;
};

struct clientinfo {
  int cn;
  bool connected;
  serverentry server;
};

struct client {
  int type;
  int num;
  ENetPeer *peer;
  string hostname;
  void *info;
};

extern void process(ENetPacket *packet, int sender, int chan);
extern client &addclient(int type);
extern void clientconnect(int n, uint ip);
extern void delclient(client *c);
extern void clientdisconnect(int n);



