CXXFLAGS= -O3 -fomit-frame-pointer
override CXXFLAGS+= -Wall -fsigned-char -fno-exceptions -fno-rtti

PLATFORM= $(shell uname -s)
PLATFORM_PREFIX= native

INCLUDES= -Ishared -Iengine -Ienet/include

STRIP=
ifeq (,$(findstring -g,$(CXXFLAGS)))
ifeq (,$(findstring -pg,$(CXXFLAGS)))
  STRIP=strip
endif
endif

ACC_INCLUDES= -DSTANDALONE $(INCLUDES)
ACC_LIBS= -Lenet/.libs -lenet -lz

ACC_OBJS= \
	shared/crypto-standalone.o \
	shared/stream-standalone.o \
	shared/tools-standalone.o \
	engine/command-standalone.o \
	parse-standalone.o \
	accserver-standalone.o

default: all

all: acc

enet/Makefile:
	cd enet; ./configure --enable-shared=no --enable-static=yes

libenet: enet/Makefile
	$(MAKE)	-C enet/ all

clean-enet: enet/Makefile
	$(MAKE) -C enet/ distclean

clean:
	-$(RM) $(ACC_OBJS) accserver

%-standalone.o: %.cpp
	$(CXX) $(CXXFLAGS) $(ACC_INCLUDES) -c -o $@ $(subst -standalone.o,.cpp,$@)

acc: libenet $(ACC_OBJS)
	$(CXX) $(CXXFLAGS) $(ACC_INCLUDES) -o accserver $(ACC_OBJS) $(ACC_LIBS)
	$(STRIP) accserver

# DO NOT DELETE

shared/crypto-standalone.o: shared/cube.h shared/tools.h shared/geom.h
shared/crypto-standalone.o: shared/ents.h shared/command.h shared/iengine.h
shared/stream-standalone.o: shared/cube.h shared/tools.h shared/geom.h
shared/stream-standalone.o: shared/ents.h shared/command.h shared/iengine.h
shared/tools-standalone.o: shared/cube.h shared/tools.h shared/geom.h
shared/tools-standalone.o: shared/ents.h shared/command.h shared/iengine.h
engine/command-standalone.o: engine/engine.h shared/cube.h shared/tools.h
engine/command-standalone.o: shared/geom.h shared/ents.h shared/command.h
engine/command-standalone.o: shared/iengine.h engine/world.h

accserver-standalone.o: shared/cube.h shared/tools.h shared/geom.h
accserver-standalone.o: shared/ents.h shared/command.h shared/iengine.h
accserver-standalone.o: parse.h
parse-standalone.o: shared/cube.h shared/tools.h shared/geom.h
parse-standalone.o: shared/ents.h shared/command.h shared/iengine.h
parse-standalone.o: parse.h
