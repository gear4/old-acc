#include "cube.h"
#include <signal.h>
#include <enet/time.h>

#include "config.h"
#include "parse.h"

FILE *logfile = NULL;

ENetAddress address;
ENetHost *server;
ENetPacket *packet;

time_t starttime;
enet_uint32 servtime = 0;

void fatal(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(logfile, fmt, args);
    fputc('\n', logfile);
    va_end(args);
    exit(EXIT_FAILURE);
}

void conoutfv(int type, const char *fmt, va_list args)
{
    vfprintf(logfile, fmt, args);
    fputc('\n', logfile);
}

void conoutf(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    conoutfv(CON_INFO, fmt, args);
    va_end(args);
}

void conoutf(int type, const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    conoutfv(type, fmt, args);
    va_end(args);
}

void slice() {
  ENetEvent event;
  bool serviced = false;
  while (!serviced) {
    if (enet_host_check_events(server, &event) <= 0) {
      if (enet_host_service(server, &event, 5) <= 0) break;
      serviced = true;
    }
    switch (event.type) {
      case ENET_EVENT_TYPE_CONNECT: {
        client &c = addclient(ST_TCPIP);
        c.peer = event.peer;
        c.peer->data = &c;
        string hn;
        copystring(c.hostname, (enet_address_get_host_ip(&c.peer->address, hn,
                                                         sizeof(hn)) == 0)
                                   ? hn
                                   : "unknown");
        printf("client connected (%s / %i)\n", c.hostname, c.num);
        clientconnect(c.num, c.peer->address.host);
      } break;

      case ENET_EVENT_TYPE_RECEIVE: {
        client *c = (client *)event.peer->data;
        if (c) process(event.packet, c->num, event.channelID);
        if (event.packet->referenceCount == 0)
          enet_packet_destroy(event.packet);
      } break;

      case ENET_EVENT_TYPE_DISCONNECT: {
        client *c = (client *)event.peer->data;
        if (!c) break;
        printf("disconnected client (%s)\n", c->hostname);
        delclient(c);
      } break;

      default:
        break;
    }
  }

  enet_host_flush(server);
}

volatile bool reloadcfg = true;

void reloadsignal(int signum)
{
    reloadcfg = true;
}

int main(int argc, char **argv)
{
  if (enet_initialize() != 0) { fatal("Could not initialize enet."); }
  atexit(enet_deinitialize);

  address.host = ENET_HOST_ANY;
  address.port = 1234;

  server = enet_host_create(&address, 100, 2, 0, 0);

  if (server == NULL) {
    printf("Could not start server.\n");
    return 0;
  }

  string logname = "acc.log";
  string cfgname = "acc.cfg";
  path(logname);
  path(cfgname);
  logfile = fopen(logname, "a");
  if(!logfile) logfile = stdout;
  setvbuf(logfile, NULL, _IOLBF, BUFSIZ);
  signal(SIGUSR1, reloadsignal);

  printf("server started\n");

  for (;;) {
    if(reloadcfg) {
        conoutf("reloading acc.cfg");
        execfile(cfgname);
        reloadcfg = 0;
    }
    servtime = enet_time_get();
    slice();
  }

  enet_host_destroy(server);
  enet_deinitialize();

  return EXIT_SUCCESS;
}

